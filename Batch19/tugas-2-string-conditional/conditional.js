// TUGAS 2 Conditional

// Condition
var nama = "Jumats";
var peran = "Penyihir";

if ( nama == "" && peran == "" ) {
    console.log("Nama Harus Diisi!");
} else if ( nama == nama && peran == "" ) {
    console.log("Halo" , nama , "Silahkan Memilih Peran Anda, untuk Memilai Game");
} else if ( nama == "" && peran == peran ){
    console.log("Halo" , peran , "Sebutkan Nama Anda");
} else if ( nama == nama && peran == peran ){
    console.log("Selamat Datang di Dunia WhereWolf," , nama);
    console.log("Halo" , peran , nama , ", kamu dapat melihat siapa yang menjadi werewolf!");
} else {
    console.log("Gunakan Nama Lain");
}

// Switch Case

var tanggal = 21;
var bulan = 1;
var tahun = 1945;

switch (bulan) {
    case 1: { console.log(tanggal , 'Januari' , tahun); break; }
    case 2: { console.log(tanggal , 'Februari' , tahun); break; }
    case 3: { console.log(tanggal , 'Maret' , tahun); break; }
    case 4: { console.log(tanggal , 'April' , tahun); break; }
    case 5: { console.log(tanggal , 'Mei' , tahun); break; }
    case 6: { console.log(tanggal , 'Juni' , tahun); break; }
    case 7: { console.log(tanggal , 'Juli' , tahun); break; }
    case 8: { console.log(tanggal , 'Agustus' , tahun); break; }
    case 9: { console.log(tanggal , 'September' , tahun); break; }
    case 10: { console.log(tanggal , 'Oktober' , tahun); break; }
    case 11: { console.log(tanggal , 'November' , tahun); break; }
    case 12: { console.log(tanggal , 'Desember' , tahun); break; }
    default : { console.log("Masukkan Bulan 1-12"); break; }
}
