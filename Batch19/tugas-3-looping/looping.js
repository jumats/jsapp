console.log("--------------------\nTUGAS 3 LOOPING\n--------------------\n")
console.log("TUGAS No. 1 Looping While\n--------------------")
var loopingWhile = 2;
var limit1 = 20;
var limit2 = 0;
var kelipatan = 2;

console.log('LOOPING PERTAMA');
while (loopingWhile <= limit1) {
  console.log(loopingWhile + ' - I Love Coding')
  loopingWhile += kelipatan;
}

console.log('LOOPING KEDUA')
loopingWhile = loopingWhile - 2;
while (loopingWhile > limit2) {
  console.log(loopingWhile + ' - I will become a mobile developer')
  loopingWhile -= kelipatan;
}

console.log("\nTUGAS No. 2 Looping menggunakan for\n--------------------")
var nilai = 1;
    var akhir = 20;
        while( nilai <= akhir ){
            if(nilai%2==0) { 
                console.log(nilai+" - Berkualitas" );
            } else if (nilai%3==0) { 
                console.log(nilai+" - I Love Coding" );
            } else { 
                console.log(nilai+" - Santai" );
            }
            nilai++;
        }

console.log("\nTUGAS No. 3 Membuat Persegi Panjang\n--------------------")
var row = 4;

for (var i = 0; i < row; i++) {
	var result = '';

	for (var j = 0; j < 8; j++) {
		if (i % 2 === 0) {
			result += '#';
		} else {
			result += '#';
		}
	}
	console.log(result);
}

console.log("\nTUGAS No. 4 Membuat Tangga\n--------------------")
var row = 7;

for (var i = 1; i <= row; i++) {
	var result = '';
	for (var j = 1; j <= i; j++) {
		result += "#";
	}
	console.log(result);
}

console.log("\nTUGAS No. 5 Membuat Papan Catur\n--------------------")
let catur = "";
let ukuran = 8;
for(let i=0 ;i<ukuran; i++){
  for(let j=0; j<ukuran; j++){
      if(i%2==1){
        if(j%2==0){
          catur = catur + "#";
        } else {
          catur = catur + " ";
        }
      } else {
          if( j%2==0){
            catur = catur + " ";
          } else {
            catur = catur + "#";
        }
      }
  }
  catur = catur + "";
  console.log(catur);
  catur = "" ;
}