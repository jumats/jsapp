// Belajar React Native at sanbercode.com
// start Soal No. 1
console.log("Soal No. 1 Mengubah fungsi menjadi fungsi arrow --------------------- ")
golden = () => {
    console.log("this is golden!!")
}
   
golden()
// end Soal No.1

// Belajar React Native at sanbercode.com
// start Soal No. 2
console.log("\nSoal No. 2 Sederhanakan menjadi Object literal di ES6 --------------------- ")

const newFunction = (firstName, lastName) => {
    return {
        firstName,lastName,
        fullName(){
            console.log(firstName + " " + lastName)
            return
        }
    }
}
newFunction("William","imoh").fullName();
// end Soal No.2

// Belajar React Native at sanbercode.com
// start Soal No. 3
console.log("\nSoal No. 3 Destructuring --------------------- ")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
 
const { firstName, lastName, destination, occupation, spell } = newObject;

console.log(firstName, lastName, destination, occupation, spell);
// end Soal No.3

// Belajar React Native at sanbercode.com
// start Soal No. 4
console.log("\nSoal No. 4 Array Spreading --------------------- ")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east];

console.log(combined);
// end Soal No.4

// Belajar React Native at sanbercode.com
// start Soal No. 5
console.log("\nSoal No. 5 Template Literals --------------------- ")

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, 
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim
    ad minim veniam`

console.log(before);
// end Soal No.5