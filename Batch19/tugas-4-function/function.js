console.log("--------------------\nTUGAS 4 FUNCTION\n--------------------\n")
console.log("TUGAS No. 1\n--------------------")
function teriak() {
    return ("Halo Sanbers!")
}
console.log(teriak())

console.log("\nTUGAS No. 2\n--------------------")
function kalikan(num1, num2){
    return num1 * num2;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali)

console.log("\nTUGAS No. 3\n--------------------")
function introduce(name, age, address, hobby){
    return "Nama saya "+ name+ ", Umur saya "+ age+ 
    " tahun, Alamat saya di "+ address+ " dan saya punya hobby yaitu "+ hobby+ "!";
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)