// Belajar React Native at sanbercode.com
console.log("Soal No. 2 (Promise Baca Buku) --------------------- ")

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

async function asyncCallFunction(){
    
    let timeLimit = 10000;

    for(let i = 0; i < books.length; i++){
        timeLimit = await readBooksPromise(timeLimit,books[i])
        .then(function(sisaWaktu){
            return sisaWaktu
        })
        .catch(function(sisaWaktu){
            return sisaWaktu;
        })
    }
    console.log("Selesai Membaca");
}

asyncCallFunction();