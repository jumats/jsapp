// Belajar React Native at sanbercode.com
console.log("Soal No. 1 (Callback Baca Buku) --------------------- ")

var readBooks = require('./callback.js')

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
let timeLimit = 10000;
let i = 0;

function callFunction(){
    
    readBooks(timeLimit, books[i], function(sisaWaktu){
        timeLimit = sisaWaktu, i++
        if(i < books.length)
        callFunction()
    })
}

callFunction();