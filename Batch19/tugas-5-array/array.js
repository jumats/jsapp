console.log("SOAL No. 1 (Range)")
function range(startNum, finishNum) {
    var rangeArr = [];

    if(startNum > finishNum){
        var rangeLength = startNum - finishNum + 1;
        for ( var i = 0; i < rangeLength; i++){
            rangeArr.push(startNum - i)
        }
    } else if ( startNum < finishNum ){
        var rangeLength = finishNum - startNum + 1;
        for ( var i = 0; i < rangeLength; i++){
            rangeArr.push(startNum + i)
        }
    } else if ( !startNum || !finishNum) {
        return -1;
    }
    return rangeArr;
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1

console.log("\nSOAL No. 2 (Range with Step)")
function rangeWithStep(startNum, finishNum, step) {
    var rangeArr = [];

    if(startNum > finishNum){
        var currentNum = startNum;
        for ( var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= step;
        }
    } else if ( startNum < finishNum ){
        var currentNum = startNum;
        for ( var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum += step
        }
    } else if ( !startNum || !finishNum || !step) {
        return -1;
    }
    return rangeArr;
}

console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

console.log("\nSOAL No. 3 (Sum of Range)")
function sum(startNum, finishNum, step){
    var rangeArr = [];
    
    if(startNum > finishNum && !step){
        var currentNum = startNum;
        for ( var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= 1;
        }
    } else if ( startNum < finishNum && !step){
        var currentNum = startNum;
        for ( var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum += 1
        }
    } else if(startNum > finishNum){
        var currentNum = startNum;
        for ( var i = 0; currentNum >= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -= step;
        }
    } else if ( startNum < finishNum){
        var currentNum = startNum;
        for ( var i = 0; currentNum <= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum += step;
        }
    } else if ( startNum && !finishNum && !step) {
        var currentNum = startNum;
            rangeArr.push(currentNum)
    } else if ( !startNum || !finishNum || !step){
        return 0;
    }
   
    const add = (a, b) =>
    a + b
    const sum = rangeArr.reduce(add)
    return sum;
}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log("\nSOAL No. 4 (Array Multidimensi)")
function dataHandling(){
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ];

    for (let i = 0; i < input.length; i++) {
    var innerArrayLength = input[i].length;
        for (let j = 0; j < innerArrayLength; j++) {
            
            if (j == 0){
                var id = input[i][j];
            }
            if (j == 1){
                var nama = input[i][j];
            }
            if (j == 2){
                var tempat = input[i][j];
            }
            if (j == 3){
                var tanggal = input[i][j];
            }
            if (j == 4){
                var hobi =  input[i][j];
            }

            var data = 
            "Nomor ID : " + id + 
            "\nNama : " + nama + 
            "\nTTL : " + tempat + " " + tanggal + 
            "\nHobi : " + hobi + "\n";

        }
        console.log(data);
    }
    return "";
}
console.log(dataHandling());

console.log("SOAL No. 5 (Balik Kata)")

function balikKata(str) {
    var strKata = str;
    var pakeString = '';

    for (let i = str.length - 1; i >= 0; i--) {
        pakeString = pakeString + strKata[i];
    }
        return pakeString;
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


console.log("\nSOAL No. 6 (Metode Array)")

function dataHandling2(){
    var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
    input.pop();
    input[1] = input[1] + "Elsharawy";
    // var editNama = "Roman Alamsyah Elsharawy" (kalau menggunakan sistem edit Profile)
    // input[1] = editNama;
    input.push("Pria", "SMA Internasional Metro")
    input[2] = "Provinsi " + input[2];
    var biodata = input;
    var bulan = input[3].split("/");
    var bulanDesc = bulan;
    var bulanAsc = bulan;
    console.log(biodata)

    var namaBulanSTR = bulan[1];
    switch (namaBulanSTR) {
        case "01": { console.log('Januari'); break; }
        case "02": { console.log('Februari'); break; }
        case "03": { console.log('Maret'); break; }
        case "04": { console.log('April'); break; }
        case "05": { console.log('Mei'); break; }
        case "06": { console.log('Juni'); break; }
        case "07": { console.log('Juli'); break; }
        case "08": { console.log('Agustus'); break; }
        case "09": { console.log('September'); break; }
        case "10": { console.log('Oktober'); break; }
        case "11": { console.log('November'); break; }
        case "12": { console.log('Desember'); break; }
        default : { console.log("Masukkan Bulan 1-12"); break; }
    }

    bulanDesc.sort(function (value1, value2) { return value2 - value1 } ) ; 
    console.log(bulanDesc)

    bulanAsc.sort(function (value1, value2) { return value1 - value2 } ) ; 
    bulanSlug = bulanAsc.join("-");
    console.log(bulanSlug)

    // var nama = input[1].substr(0, 15); // jika menggunakan ini akan jelek ketika nama yg di input Muhammad Muzakkir akan menjadi "muhammad muzakk"
    
    var getName = input[1].split(" ");
    var SliceNama = getName.slice(0,2);
    var Nama = SliceNama.join(" ");
    console.log(Nama)


    return input;
}
dataHandling2();