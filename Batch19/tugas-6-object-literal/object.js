// Belajar React Native at sanbercode.com
// start Soal No. 1
console.log("Soal No. 1 (Array to Object) --------------------- ")
function arrayToObject(arr){
    for(var i=0; i<arr.length; i++){

        var data = {};
        var birthYear = arr[i][3];
        var now = new Date();
        var thisYear = now.getFullYear();

        var age;
        if (birthYear && thisYear - birthYear > 0){
            age = thisYear - birthYear;
        } else {
            age = "Invalid Birth Year";
        }

        data.firstName = arr[i][0];
        data.lastName = arr[i][1];
        data.gender = arr[i][2];
        data.age = age;

        var num = i+1;
        var infoData = num + '. ' + data.firstName + ' ' + data.lastName + ' : '

        console.log(infoData);
        console.log(data);
    }

    if(arr.length <= 0){
        return console.log("Tidak ada Data yang ditampilkan")
    }

    return arr;
}
// console.log("----- Input -----")
// var input = [ ["Abduh", "Muhamad", "male", 1992], ["Ahmad", "Taufik", "male", 1985] ];
// arrayToObject(input)

console.log("----- People1 -----")
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ];
arrayToObject(people)

console.log("----- People2 -----")
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ];
arrayToObject(people2) 

//arrayToObject([]); //error Case
// end Soal No.1

// Belajar React Native at sanbercode.com
// start Soal No. 2
console.log("\nSoal No. 2 (Shopping Time) --------------------- ")
function shoppingTime(memberId, money) {

    var minimumMoney = 50000;
    var hargaStacattu = 1500000;
    var hargaBajuZoro = 500000;
    var hargaBajuHN = 250000;
    var hargasweaterUniklooh = 175000;
    var hargaCasingHP = 50000;

    if(!memberId){
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    } else if (money < minimumMoney){
        return "Mohon Maaf, Uang tidak Cukup";
    } else {
        var newObject = {};
        var myMoney = money;
        var availablePurchase = [];

        var sepatuStacattu = "Sepatu Stacatu";
        var bajuZoro = "Baju Zoro";
        var bajuHN = "Baju H&N";
        var sweaterUniklooh = "Sweater Uniklooh";
        var casingHandphone = "Casing Handphone";

        var check = 0;
        for (var i = 0; myMoney >= minimumMoney && check == 0; i++){
            if(myMoney >= hargaStacattu){
                availablePurchase.push(sepatuStacattu);
                myMoney -= hargaStacattu;
            } else if (myMoney >= hargaBajuZoro){
                availablePurchase.push(bajuZoro);
                myMoney -= hargaBajuZoro;
            } else if (myMoney >= hargaBajuHN){
                availablePurchase.push(bajuHN);
                myMoney -= hargaBajuHN;
            } else if (myMoney >= hargasweaterUniklooh){
                availablePurchase.push(sweaterUniklooh);
                myMoney -= hargasweaterUniklooh;
            } else if (myMoney >= minimumMoney){
                for(var j = 0; j <= availablePurchase.length - 1; j++){
                    if(availablePurchase[j] == casingHandphone){
                        check += 1
                    }
                }

                if(check == 0){
                    availablePurchase.push(casingHandphone);
                    myMoney -= hargaCasingHP;
                }else{
                    availablePurchase.push(casingHandphone);
                    myMoney -= hargaCasingHP;
                }
            }
        }

        newObject.memberId = memberId
        newObject.money = money
        newObject.listPurchased = availablePurchase
        newObject.changeMoney = myMoney

        return newObject;
    }
}

// console.log(shoppingTime('324193hDew2', 700000));
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());
// end Soal No.2

// Belajar React Native at sanbercode.com
// start Soal No. 3
console.log("\nSoal No. 3 (Naik Angkot) --------------------- ")
function naikAngkot(arrPenumpang){
    var rute = ["A","B","C","D","E","F"];
    var penumpang = [];
    var ongkos = 2000;

    if(arrPenumpang.length <= 0){
        return [];
    }

    for(var i = 0; i < arrPenumpang.length; i++){
        var objectAngkot = {};
        var halte = arrPenumpang[i][1]
        var tujuan = arrPenumpang[i][2]

        var dariHalte;
        var turunAngkot;

    for(var j = 0; j < rute.length; j++){
        if(rute[j] == halte){
            dariHalte = j;
        } else if (rute[j] == tujuan){
            turunAngkot = j;
        }
    }

    var bayar = (turunAngkot - dariHalte) * ongkos

    objectAngkot.penumpang = arrPenumpang[i][0]
    objectAngkot.naikDari = halte
    objectAngkot.tujuan = tujuan
    objectAngkot.bayar = bayar

    penumpang.push(objectAngkot)
    }

    return penumpang;
}  

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// console.log(naikAngkot([])); //[]
// end Soal No.3