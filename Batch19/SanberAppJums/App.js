import React from 'react';
// import { StyleSheet, Text, View } from 'react-native';

// import Component from './Latihan/Component/Component';
// import LatihanToggle from './Tugas/Tugas13/index';

// Tugas 12 - Component-Youtube
// import Youtube from './Tugas/Tugas12/App';

//Tugas 13 – Styling & Flexbox
// import LoginScreen from './Tugas/Tugas13/LoginScreen';
// import Registration from './Tugas/Tugas13/Registration';
// import AboutScreen from './Tugas/Tugas13/AboutScreen';

// === TUGAS 14 Component API & Lifecycle
// import NoterApp from './Tugas/Tugas14/App';

// === Tugas 15 – React Navigation
// import TutorialNavigation from './Tugas/Tugas15/Index'; // Tutorial
import TugasNavigation from './Tugas/TugasNavigation'; // Implementasi React Navigation
import Ujian from './Tugas/Quiz3';


export default function App() {
  return (
    // <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}><Text>No Problem, all Clear lets Coding!</Text></View>
    // <Component /> // Test
    // <LatihanToggle /> // Test
    
    // === TUGAS 12 ==================================
    // <Youtube />
    
    // === TUGAS 13 ==================================
    // Lihat Halaman Regist & About dengan aktifkan salah satu dibawah ini
    // <LoginScreen />
    // <Registration />
    // <AboutScreen />

    // === TUGAS 14 ==================================
    // <NoterApp />

    // === TUGAS 15 ==================================
    //  <TutorialNavigation /> // this is Tugas No.1 Tutorial React Navigation 5.x
     <Ujian />
  )
}