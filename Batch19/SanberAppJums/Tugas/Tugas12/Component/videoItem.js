import React, { Component } from 'react'
import { View, Image, StyleSheet, TouchableOpacity, Text } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class VideoItem extends Component {
    render() {
        let video = this.props.video;
        return (
            <View style={styles.container}>
                <Image source={{uri:video.snippet.thumbnails.medium.url}} style={{height: 200}} />
                <View style={styles.descContainer}>
                    <Image source={{uri:'https://randomuser.me/api/portraits/women/56.jpg'}} style={{width:50, height:50, borderRadius:50}} />
                    <View style={styles.videoDetail}>
                        <Text numberOfLines={1} style={styles.videoTitle}>{video.snippet.title}</Text>
                        <Text style={styles.videoStats}>{video.snippet.channelTitle + " · " + nFormatter(video.statistics.viewCount, 1) + " Views"}</Text>
                        
                        <View style={styles.navBar}>
                            <View style={styles.rightNav}>
                                <TouchableOpacity>
                                <Text style={styles.navItem}><Icon name="thumb-up" size={12} /> {nFormatter(video.statistics.likeCount, 1)}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity>
                                <Text style={styles.navItem}><Icon name="thumb-down" size={12} /> {nFormatter(video.statistics.dislikeCount, 1)}</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                    </View>
                    <TouchableOpacity>
                        <Icon name="more-vert" size={20} color="#999" />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

function nFormatter(num, digits) {
    var si = [
      { value: 1, symbol: "" },
      { value: 1E3, symbol: "k" },
      { value: 1E6, symbol: "M" },
      { value: 1E9, symbol: "G" },
      { value: 1E12, symbol: "T" },
      { value: 1E15, symbol: "P" },
      { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
      if (num >= si[i].value) {
        break;
      }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + "";
  }
 

const styles = StyleSheet.create({
    container: {
        padding: 15
    },
    descContainer: {
        flexDirection: 'row',
        paddingTop: 15,
    },
    videoTitle: {
        fontSize: 16,
        color: '#3c3c3c',
    },
    videoDetail: {
        paddingHorizontal: 15,
        flex: 1,
    },
    videoStats: {
        fontSize: 14,
        paddingTop: 3,
        color: '#777',
    },
    videoDescription: {
        fontSize:12,
        color: '#999',

    },

    navBar: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    rightNav: {
        flexDirection: 'row'
    },
    navItem: {
        marginRight: 15
    },
})