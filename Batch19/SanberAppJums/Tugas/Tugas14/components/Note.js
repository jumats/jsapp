import React from 'react';
import { 
  View, 
  Text, 
  TouchableOpacity, 
  StyleSheet } from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class Note extends React.Component {
  render(){
    return(
      <View style={styles.note} key={this.props.keyval1}>
        <Text style={styles.noteDateText}>{this.props.val.date}</Text>
        <Text style={styles.noteText}>{this.props.val.note}</Text>
        
        <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
          <Text style={styles.noteDeleteText}>
            <Icon name="delete" size={30} />
          </Text>
        </TouchableOpacity>
      </View>

    )
  }
};

const styles = StyleSheet.create({
  note: {
    position: 'relative',
    padding: 20,
    paddingRight: 100,
    borderBottomWidth: 2,
    borderBottomColor: '#ededed',
  },
  noteText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#029097',
    fontSize: 16,
  },
  
  noteDateText: {
    paddingLeft: 20,
    borderLeftWidth: 10,
    borderLeftColor: '#029097',
    color: '#555',
    fontSize: 12,
  },
  noteDelete: {
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    top: 10,
    bottom: 10,
    right: 10
  },
  noteDeleteText: {
    color: '#04BEC7',
  }
});

