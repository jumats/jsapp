import React from 'react';
import { 
  View, 
  Text, 
  ScrollView, 
  TextInput,
  StyleSheet, 
  TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import Note from './Note';

export default class Main extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      noteArray: [],
      noteText: '',
    }
  }
  render(){
    let notes = this.state.noteArray.map((val, key) => {
      return <Note key={key} keyval={key} val={val}
      deleteMethod ={()=> this.deleteNote(key)}></Note>
    });
    return(
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headerText}>SanberAppJums</Text>
          <Text style={styles.headerText2}>== NOTER ==</Text>
        </View>

        <ScrollView style={styles.scrollContainer}>
          {notes}
        </ScrollView>

        <View style={styles.footer}>
          <TextInput 
            style={styles.textInput}
            onChangeText={(noteText)=>this.setState({noteText})}
            value={this.state.noteText}
            placeholder="Write a notes"
            placeholderTextColor="#777777"
            underlineColorAndroid="transparent"
          ></TextInput>
        </View>
        <TouchableOpacity onPress={this.addNote.bind(this)} style={styles.addButton}>
          <Text style={styles.addButtonText}>
              <Icon style={styles.iconside} name="send" size={50} />
          </Text>
        </TouchableOpacity>

      </View>
    );
  }
  addNote() {
    if(this.state.noteText){
      var d = new Date();
      this.state.noteArray.push({
        'date': d.getFullYear() + 
        "/" + (d.getMonth() + 1) + 
        "/" + d.getDate() + "  -  " + d.getHours() + ":" + d.getMinutes(),
        'note': this.state.noteText
      });
      this.setState({noteArray: this.state.noteArray})
      this.setState({noteText: ''})
    }
  }
  deleteNote(key) {
    this.state.noteArray.splice(key, 1);
    this.setState({noteArray: this.state.noteArray})
  }

};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#04BEC7',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 10,
    borderBottomColor: '#029097',
  },
  headerText: {
    color: '#fff',
    fontSize: 15,
    paddingTop: 20,
  },
  headerText2: {
    color: '#fff',
    fontSize: 18,
    paddingBottom: 15,
    fontWeight: '700',
  },
  scrollContainer: {
    flex: 1,
    marginBottom: 100,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 10,
  },
  textInput: {
    alignSelf: 'stretch',
    color: '#000',
    paddingTop: 10,
    paddingLeft: 20,
    paddingBottom: 30,
    backgroundColor: '#eee',
    borderLeftWidth: 10,
    borderLeftColor: '#04BEC7',
    borderTopWidth: 1,
    borderTopColor: '#04BEC7',
    
  },
  addButton: {
    position: 'absolute',
    zIndex: 11,
    right: 20,
    bottom: 8,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButtonText: {
    color: '#04BEC7',
    fontSize: 24,
  },
});

