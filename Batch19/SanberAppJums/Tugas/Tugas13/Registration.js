import React, { useState } from 'react'
import { 
    View, 
    Image, 
    StyleSheet, 
    TouchableOpacity, 
    Text,
    TextInput, 
    Platform, 
    ScrollView,
    KeyboardAvoidingView,
    CheckBox} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons';

const Registration = () => {
    const [isSelected, setSelection] = useState(false);
    return (
        <KeyboardAvoidingView
        behavior ={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
        >
        <ScrollView>
        <View style={styles.containerView}>
            <View style={styles.header}>
                <View style={styles.logoElement}>
                    <Image style={styles.logoItem} source={require('./images/logo.png')}/>
                </View>
                <Text style={styles.welcomeText}>Registration!</Text>
                <Text style={styles.welcomeDesc}>Let me know Who You are?</Text>
            </View>
            
            <View style={styles.content}>
                <View style={styles.boxInput}>
                    <Icon style={styles.iconside} name="person" size={15} />
                    <TextInput placeholder = "Username" style={styles.formText} />
                </View>

                <View style={styles.boxInput}>
                    <Icon style={styles.iconside} name="mail" size={15} />
                    <TextInput placeholder = "Email" style={styles.formText} />
                </View>
                
                <View style={styles.boxInput}>
                    <Icon style={styles.iconside} name="lock" size={15} />
                    <TextInput placeholder = "Password" style={styles.formText} secureTextEntry />
                </View>

                <View style={styles.boxInput}>
                    <Icon style={styles.iconside} name="lock" size={15} />
                    <TextInput placeholder = "Re-password" style={styles.formText} secureTextEntry />
                </View>

                <View style={styles.checkForgot}>
                    <View style={styles.checkboxContainer}>
                        <CheckBox
                        value={isSelected}
                        onValueChange={setSelection}
                        style={styles.checkbox}
                        />
                        <Text style={styles.label}>Agree With: </Text>
                        <TouchableOpacity>
                            <Text style={styles.labelForgot}>Terms and Conditions</Text>
                        </TouchableOpacity>
                    </View>
                    
                </View>
                <View style={styles.loginContainer}>
                    <TouchableOpacity style={styles.btLogin}>
                        <Text style={styles.btText}>REGISTER</Text>
                    </TouchableOpacity>
                </View>


                <View>
                    <Text style={styles.haveAccount}>I'm Ready have Account</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.btRegist}>
                        <Text style={styles.btText}>LOGIN</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

export default Registration;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    containerView: {
       
    },
    header: {
        padding: 20,
        height: 270,
        backgroundColor: '#04BEC7',
        alignContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },
    logoElement: {
        width: 100,
        height: 100,
        marginTop: 50,
        backgroundColor: "#fff",
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,
    },
    logoItem: {
        width: 80,
        height: 40,
    },
    welcomeText: {
        fontSize: 30,
        fontWeight: "700",
        textAlign: 'center',
        color: '#fff',
    },
    welcomeDesc: {
        fontSize: 15,
        fontWeight: "400",
        textAlign: 'center',
        color: '#fff',
    },
    content: {
        alignItems: 'center',
        paddingTop: 70,
    },
    
    boxInput: {
        height: 40,
        width: 280,
        borderColor: '#ccc',
        borderWidth: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginVertical: 9,
    },
    iconside: {
        marginTop: 10,
        marginLeft: 10,
        marginRight: 10,
        color: '#777',
    },
    formText: {
        width: 220,
    },
    checkForgot: {
        height: 20,
        width: 280,
        flexDirection: "row",
        justifyContent: 'space-between',
        marginBottom: 5,
    },
    checkboxContainer: {
        height: 20,
        flexDirection: "row",
    },
    fogorContainer: {
        height: 20,
        flexDirection: "row",
    },
    checkbox: {
        width: 30,
        height: 18,
    },
    label: {
        fontSize: 13,
        color: '#555',
        marginTop: 1,
    },
    labelForgot: {
        fontSize: 13,
        color: '#555',
        marginTop: 1,
        textDecorationLine: 'underline',
    },
    loginContainer: {
        marginVertical: 10,
    },
    btLogin: {
        backgroundColor: '#04BEC7',
        paddingHorizontal: 40,
        paddingVertical: 10,
        borderRadius: 3,
    },
    btText: {
        color: '#fff',
        fontWeight: '700',
    },

    haveAccount: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        marginTop: 100,
        paddingHorizontal: 50,
        paddingBottom: 10,
        marginBottom: 10,
    },
    
    btRegist: {
        backgroundColor: '#04BEC7',
        paddingHorizontal: 40,
        paddingVertical: 10,
        borderRadius: 3,
    },

  })